import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import RNExitApp from 'react-native-exit-app';
import StatusBar from './common/statusbar';
import AppStatusBar from './common/appstatusbar';
import Functions from './functions/functions';
import theme from './common/theme';

export default class MainScreen extends Component {
  deletePin = async () => {
    try {
         await AsyncStorage.setItem('@PinSet', "false");
       } catch (error) {
         console.log(error)
       }
    RNExitApp.exitApp();
  }
	render() {
    	return (
    		<View style={styles.container}>
    			<StatusBar />
    			<AppStatusBar center={true} text="Native Functions" elevation={true} />
    			<ScrollView>
    				<View style={styles.mainContent}>
    					<Functions />
					</View>
	    		</ScrollView>
          <TouchableOpacity style={styles.button} onPress={this.deletePin}>
              <Text style={styles.buttonText}>Delete Pin & Exit</Text>
          </TouchableOpacity>
    		</View>
    	);
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  mainContent: {
  	alignItems: 'center',
  },
  button: {
    position: 'absolute', 
    bottom: 0,
    width: "100%",
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green'
  },
  buttonText: {
    fontSize: 20,
    color: theme.white
  }
});