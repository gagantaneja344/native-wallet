import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
const VirgilCrypto =require('virgil-crypto');
import StatusBar from '../common/statusbar';
import AppStatusBar from '../common/appstatusbar';
import theme from '../common/theme';
import Back from '../common/images/back.png';

let encryptedData;
const virgilCrypto = new VirgilCrypto.VirgilCrypto();
const encryptionKeypair = virgilCrypto.generateKeys();

export default class EncryptDecrypt extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			message: 'Hello, Bob!',
			messageHeading: "Message To Encrypt",
			buttonText: "Encrypt"
		};
		this.encryptdecrypt = this.encryptdecrypt.bind(this);
	}
	encryptdecrypt() {
		if(this.state.buttonText === "Encrypt") {
			const messageToEncrypt = 'Hello, Bob!';
			encryptedData = virgilCrypto.encrypt(messageToEncrypt, encryptionKeypair.publicKey);
			const encryptedMessage = encryptedData.toString('base64');
			this.setState({message: encryptedMessage, messageHeading: "Encrypted Message", buttonText: "Decrypt"});
		}
		else {
			const decryptedData = virgilCrypto.decrypt(encryptedData, encryptionKeypair.privateKey);
			const decryptedMessage = decryptedData.toString('utf8');
			this.setState({message: decryptedMessage, messageHeading: "Message To Encrypt", buttonText: "Encrypt"});
		}
	}
	goBack() {
		Actions.pop();
	}
	render() {
    	return (
	    	<View style={styles.container}>
	    		<StatusBar />
    			<AppStatusBar left={true} Back={Back} leftFunction={this.goBack} center={true} text="Encrypt/Decrypt" elevation={true} />
    			<View style={styles.mainContent}>
	    			<View style={styles.messageHeading}>
	    				<Text style={styles.messageHeadingText}>{this.state.messageHeading}</Text>
	    			</View>
	    			<View style={styles.messageFlex}>
	    				<Text style={styles.messageText}>{this.state.message}</Text>
	    			</View>
	    			<TouchableOpacity style={styles.button} onPress={this.encryptdecrypt}>
	    				<Text style={styles.buttonText}>{this.state.buttonText}</Text>
	    			</TouchableOpacity>
	    		</View>
    		</View>
    	);
    }
}
const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor: 'white',
	    alignItems: 'center',
	},
	mainContent: {
		flex: 0.9,
		width: '100%',
		alignItems: 'center',
	},
	messageHeading: {
		flex: 0.07,
		width: '90%',
		marginTop: 10,
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: theme.dark,
		alignItems: 'center',
		justifyContent: 'center'
	},
	messageHeadingText: {
		fontSize: 18, 
	},
	messageFlex: {
		flex: 0.8,
		paddingVertical: 10,
		width: '90%',
		alignItems: 'center',
	},
	button: {
		position: 'absolute', 
		bottom: 0,
		width: "100%",
		height: 60,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'green'
	},
	buttonText: {
		fontSize: 20,
		color: theme.white
	}
});

