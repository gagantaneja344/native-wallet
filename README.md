# Native Wallet

React Native Wallet depicting various crypto wallet functions and coin related functions.

## Getting Started

Clone the repository to get started.

### Prerequisites

Set up basic React native tools:-

* React Native CLI

* Watchman

* Node (Latest Version)

### Installing

A step by step series of examples that tell you how to get a development env running

* **Installing dependencies**

```
cd native-wallet
npm install
npm i --save react-native-crypto
npm i --save react-native-randombytes
react-native link react-native-randombytes
npm i --save-dev tradle/rn-nodeify
./node_modules/.bin/rn-nodeify --hack --install
cd coins/BTC/bitcoin-vevsa-lib
npm install
```

* **rn-nodeify will create a shim.js in the project root directory**

Open index.js in the root directory and add the following line of code :

```
import './shim.js'
```

* **Run the app in your simulator**
```
react-native run-ios
```
OR
```
react-native run-android
```

## Authors

* **Vevsa Technologies** - (https://github.com/vevsatechnologies)
* **Tarun Gupta** - (https://github.com/tarun1475)
* **Gagan Taneja** - (https://github.com/gagantaneja)
