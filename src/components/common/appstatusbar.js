import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import ElevatedView from 'react-native-elevated-view';
import theme from './theme';

export default class AppStatusBar extends Component {
	render() {
		let elevation = 0;
		if(this.props.elevation) {
			elevation = 3
		}

		return (
			<ElevatedView style={styles.statusBar} elevation={elevation}>
				<TouchableOpacity style={styles.iconContainer} onPress={this.props.leftFunction}>
					{ this.props.left ? <Image style={styles.iconImage} source={this.props.Back} /> : null}
				</TouchableOpacity>
				<View style={styles.centerTextContainer}>
					{ this.props.center ? <Text style={styles.centerText}>{this.props.text}</Text> : null}
				</View>
				<TouchableOpacity style={styles.iconContainer} onPress={this.props.rightFunction}>
					{ this.props.right ? <Image style={styles.iconImage} source={this.props.Forward} /> : null}
				</TouchableOpacity>
			</ElevatedView>
		);
	}
}
const styles = StyleSheet.create({
	statusBar: {
		backgroundColor: theme.white,
		flex: 0.1,
		width: "100%",
		alignItems: 'center',
		flexDirection: 'row'
	},
	iconContainer: {
		flex: 1.3 / 6,
		alignItems: 'center',
	},
	centerTextContainer: {
		flex: 3.4 / 6,
		alignItems: 'center'
	},
	centerText: {
		fontFamily: theme.font,
		fontSize: 20
	},
	iconImage: {
		width: 30,
		height: 30
	}
})
