import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView, AsyncStorage, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import StatusBar from '../statusbar';
import AppStatusBar from '../appstatusbar';
import theme from '../theme';
import Next from '../images/next.png';

export default class CreatePin extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			pinCode: "",
		};
	}
	gotoConfirmPin = () => {
		const pin = this.state.pinCode;
		if(pin.length !== 4) {
			alert("Enter 4 digit Pin");
		}
 		else {
 			Actions.confirmpin({pinCode: this.state.pinCode});
 		}	
	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.headingFlex}>
					<Text style={styles.headingText}>Create Pin</Text>
				</View>
				<View style={styles.inputFlex}>
					<TextInput 
						style={styles.inputText}
	    				secureTextEntry
	    				keyboardAppearance="dark"
	    				keyboardType="number-pad"
	    				value={this.state.savedCode}
	    				maxLength={4}
	    				placeholder="X-X-X-X"
	    				placeholderTextColor="rgba(0,0,0,0.3)"
	    				onChangeText={(text) => this.setState({pinCode: text})}
					/>
				</View>
				<View style={styles.buttonFlex}>
					<TouchableOpacity onPress={this.gotoConfirmPin}>
						<Image source={Next} style={styles.nextButton} />
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor: 'white',
	},
	headingFlex: {
		flex: 0.25,
		alignItems: 'center',
		justifyContent: 'center'
	},
	headingText: {
		fontSize: 40,
	},
	inputFlex: {
		flex: 0.15,
		alignItems: 'center',
		justifyContent: 'center'
	},
	inputText: {
		borderBottomColor: "rgba(0,0,0,0.8)",
		borderBottomWidth: 3,
		textAlign: 'center',
		fontSize: 35,
		width: '50%',
		color: theme.dark
	},
	buttonFlex: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	nextButton: {
		width: 50,
		height: 50
	}
});