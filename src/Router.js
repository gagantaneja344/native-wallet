import React from 'react';
import { Image, View, Dimensions, StyleSheet } from 'react-native';
import { Scene, Router, Stack } from 'react-native-router-flux';
import HomeScreen from './components/homescreen';
import MainScreen from './components/mainscreen';
import GenerateMnemonic from './components/functions/generatemnemonic';
import EncryptDecrypt from './components/functions/encryptDecrypt';
import BTCKeys from './components/functions/btckeys';
import BTCTransaction from './components/functions/btctransaction';
import CreatePin from './components/common/pin/createpin';
import ConfirmPin from './components/common/pin/confirmpin';
import EnterPin from './components/common/pin/enterpin';

const RouterComponent = () => {
	return (
		<Router>
		 	<Scene key="root">
		 		<Scene hideNavBar key="homescreen" component={HomeScreen} />
		 		<Scene hideNavBar key="mainscreen" component={MainScreen} />
		 		<Scene hideNavBar key="generatemnemonic" component={GenerateMnemonic} />
		 		<Scene hideNavBar key="encryptdecrypt" component={EncryptDecrypt} />
		 		<Scene hideNavBar key="btckeys" component={BTCKeys} />
		 		<Scene hideNavBar key="btctransaction" component={BTCTransaction} />
		 		<Scene hideNavBar key="createpin" component={CreatePin} />
		 		<Scene hideNavBar key="confirmpin" component={ConfirmPin} />
		 		<Scene hideNavBar key="enterpin" component={EnterPin} />
		 	</Scene>
		</Router>
	);
}
export default RouterComponent;