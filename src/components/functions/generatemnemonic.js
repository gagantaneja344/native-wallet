import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
const VirgilCrypto =require('virgil-crypto');
const bip39 = require('react-native-bip39');
import StatusBar from '../common/statusbar';
import AppStatusBar from '../common/appstatusbar';
import theme from '../common/theme';
import Back from '../common/images/back.png';

export default class GenerateMnemonic extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			mnemonic: "",
			seed: "",
			publicKey: "",
			privateKey: ""
		};
		this.generateMnemonic = this.generateMnemonic.bind(this);
	}
	generateMnemonic() {
		const virgilCrypto = new VirgilCrypto.VirgilCrypto();
		const promise = bip39.generateMnemonic();
			promise.then((result)=>{
			  const mnemonic = result;
			  const seed = bip39.mnemonicToSeedHex(result);
			  const keyPair = virgilCrypto.generateKeysFromKeyMaterial(seed);
			  const privateKeyData = virgilCrypto.exportPrivateKey(keyPair.privateKey);
			  const publicKeyData = virgilCrypto.exportPublicKey(keyPair.publicKey);
			  const privateKey=privateKeyData.toString('base64');
			  const publicKey=publicKeyData.toString('base64');
			  this.setState({mnemonic, seed, privateKey, publicKey})
			})
	}
	goBack() {
		Actions.pop();
	}
	render() {
    	return (
	    	<View style={styles.container}>
	    		<StatusBar />
    			<AppStatusBar left={true} Back={Back} leftFunction={this.goBack} center={true} text="Generate Mnemonic" elevation={true} />
    			<ScrollView>
	    			<View style={styles.mainContent}>
	    				<View style={styles.section}>
	    					<View style={styles.sectionHeading}>
	    						<Text style={styles.sectionHeadingText}>Mnemonic</Text>
	    					</View>
	    					<View style={styles.sectionContent}>
	    						<Text style={styles.sectionContentText}>{this.state.mnemonic}</Text>
	    					</View>
	    				</View>
	    				<View style={styles.section}>
	    					<View style={styles.sectionHeading}>
	    						<Text style={styles.sectionHeadingText}>Seed</Text>
	    					</View>
	    					<View style={styles.sectionContent}>
	    						<Text style={styles.sectionContentText}>{this.state.seed}</Text>
	    					</View>
	    				</View>
	    				<View style={styles.section}>
	    					<View style={styles.sectionHeading}>
	    						<Text style={styles.sectionHeadingText}>Public Key</Text>
	    					</View>
	    					<View style={styles.sectionContent}>
	    						<Text style={styles.sectionContentText}>{this.state.publicKey}</Text>
	    					</View>
	    				</View>
	    				<View style={styles.section}>
	    					<View style={styles.sectionHeading}>
	    						<Text style={styles.sectionHeadingText}>Private Key</Text>
	    					</View>
	    					<View style={styles.sectionContent}>
	    						<Text style={styles.sectionContentText}>{this.state.privateKey}</Text>
	    					</View>
	    				</View>
	    			</View>
	    		</ScrollView>
	    		<TouchableOpacity style={styles.generateButton} onPress={this.generateMnemonic}>
	    			<Text style={styles.buttonText}>Generate New Pair</Text>
	    		</TouchableOpacity>
	    	</View>
    	);
    }
}
const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor: 'white',
	},
	mainContent: {
		flex: 0.9,
		width: "100%"
	},
	generateButton: {
		width: '100%',
		height: 60,
		backgroundColor: 'green',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontSize: 20,
		color: theme.white
	},
	section: {
		marginTop: 10,
		width: '100%',
		alignItems: 'center'
	},
	sectionHeading: {
		height: 30,
		width: '100%',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: theme.dark,
		justifyContent: 'center',
		alignItems: 'center'
	},
	sectionText: {
		fontSize: 20,
		color: theme.dark
	},
	sectionContent: {
		paddingVertical: 10,
		width: '90%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	sectionContentText: {
		fontSize: 16,
		color: theme.dark
	}
});