import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import StatusBar from '../common/statusbar';
import AppStatusBar from '../common/appstatusbar';
import theme from '../common/theme';
import Back from '../common/images/back.png';
var bitcoin = require('../../../coins/BTC/bitcoin-vevsa-lib');

export default class BTCKeys extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			addressStr: "",
			publicKeyStr: "",
			privateKeyStr: ""
		};
		this.generateKeys = this.generateKeys.bind(this);
	}
	goBack() {
		Actions.pop();
	}
	generateKeys() {
		/*
		* Returns a randomly generated pirvate key
		*
		*  Instantiate a PrivateKey 
		*
		* @return
		*    A private key generated from a BN , Buffer and WIF.
		*/

		var privateKey = new bitcoin.PrivateKey(bitcoin.Networks.testnet);
		const privateKeyStr = privateKey.toString();



		/**
		 * Returns the corresponding public key generated from the private key
		 *
		 * instance method of private key 
		 * 
		 * @returns
		 *    A public key generated from private key
		 */

		var publicKey  = privateKey.toPublicKey();
		const publicKeyStr = publicKey.toString();



		/**
		 * Returns an address for the public key
		 * 
		 * @param {String | Network} bitcoin.Networks.livenet - which network should be the address for
		 *
		 * @returns {Address}
		 *    An address generated from the public key.
		 */


		var address = publicKey.toAddress(bitcoin.Networks.testnet);
		const addressStr = address.toString();
		this.setState({addressStr, publicKeyStr, privateKeyStr})

	}
	render() {
    	return (
	    	<View style={styles.container}>
	    		<StatusBar />
    			<AppStatusBar left={true} Back={Back} leftFunction={this.goBack} center={true} text="Bitcoin Keys" elevation={true} />
    			<ScrollView>
	    			<View style={styles.mainContent}>
	    				<View style={styles.section}>
		    				<View style={styles.sectionHeading}>
		    					<Text style={styles.sectionHeadingText}>Bitcoin Public Address</Text>
		    				</View>
		    				<View style={styles.sectionContent}>
		    					<Text style={styles.sectionContentText}>{this.state.addressStr}</Text>
		    				</View>
		    			</View>
		    			<View style={styles.section}>
		    				<View style={styles.sectionHeading}>
		    					<Text style={styles.sectionHeadingText}>Public Key</Text>
		    				</View>
		    				<View style={styles.sectionContent}>
		    					<Text style={styles.sectionContentText}>{this.state.publicKeyStr}</Text>
		    				</View>
		    			</View>
		    			<View style={styles.section}>
		    				<View style={styles.sectionHeading}>
		    					<Text style={styles.sectionHeadingText}>Private Key</Text>
		    				</View>
		    				<View style={styles.sectionContent}>
		    					<Text style={styles.sectionContentText}>{this.state.privateKeyStr}</Text>
		    				</View>
		    			</View>
	    			</View>
    			</ScrollView>
    			<TouchableOpacity style={styles.generateButton} onPress={this.generateKeys}>
	    			<Text style={styles.buttonText}>Generate New Keys</Text>
	    		</TouchableOpacity>
    		</View>
    	);
    }
}
const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor: 'white',
	},
	mainContent: {
		flex: 0.9,
		width: "100%"
	},
	generateButton: {
		width: '100%',
		height: 60,
		backgroundColor: 'green',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontSize: 20,
		color: theme.white
	},
	section: {
		marginTop: 10,
		width: '100%',
		alignItems: 'center'
	},
	sectionHeading: {
		height: 30,
		width: '100%',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: theme.dark,
		justifyContent: 'center',
		alignItems: 'center'
	},
	sectionText: {
		fontSize: 20,
		color: theme.dark
	},
	sectionContent: {
		paddingVertical: 10,
		width: '90%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	sectionContentText: {
		fontSize: 16,
		color: theme.dark
	}
});