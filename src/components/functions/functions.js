import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Accordion from 'react-native-collapsible/Accordion';
import { Actions } from 'react-native-router-flux';
import theme from '../common/theme';
import Up from '../common/images/up.png';
import Down from '../common/images/down.png';

const CONTENT = [
	{
		number: 1,
		name: "Generate Mnemonic",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam interdum ante vitae lacus commodo, at consectetur enim finibus. Ut id eleifend ligula."
	},
	{
		number: 2,
		name: "Encrypt & Decrypt Data",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam interdum ante vitae lacus commodo, at consectetur enim finibus. Ut id eleifend ligula."
	},
	{
		number: 3,
		name: "Bitcoin Keys",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam interdum ante vitae lacus commodo, at consectetur enim finibus. Ut id eleifend ligula."
	},
	{
		number: 4,
		name: "Bitcoin Transaction",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam interdum ante vitae lacus commodo, at consectetur enim finibus. Ut id eleifend ligula."
	},
]
export default class Functions extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			activeSection: false,
			collapsed: true
		};
	}
	setSection = section => {
		this.setState({ activeSection: section });
	};
	toggleExpanded = () => {
		this.setState({ collapsed: !this.state.collapsed });
	};

	renderHeader = (section, _, isActive) => {
		return (
		<Animatable.View
			duration={300}
			style={styles.sectionHeader}
			transition="backgroundColor"
		>
			<View style={styles.functionHeading}>
				<Text style={styles.functionHeadingText}>{section.name}</Text>
			</View>
			<View style={styles.headingIconContainer}>
				<Image style={styles.headingIcon} source={isActive ? Up : Down} />
			</View>
		</Animatable.View>
		);
	};

	renderContent(section, _, isActive) {
		const runFunction = (activeSection) => {
			if(activeSection === 1) {
				Actions.generatemnemonic();
			}
			else if(activeSection === 2) {
				Actions.encryptdecrypt();
			}
			else if(activeSection === 3) {
				Actions.btckeys();
			}
			else if(activeSection === 3) {
				Actions.btckeys();
			}
			else {
				Actions.btctransaction();
			}
		}
		return (
			<Animatable.View
				duration={300}
				style={styles.sectionContent}
				transition="backgroundColor"
			>
				<View style={styles.functionDescription}>
					<Text style={styles.functionDescriptionText}>{section.description}</Text>
				</View>
				<View style={styles.buttonFlex}>
					<TouchableOpacity style={styles.runButton} onPress={() => {runFunction(section.number)}}>
						<Text style={styles.runText}>Run</Text>
					</TouchableOpacity>
				</View>
			</Animatable.View>
		);
	};
	render() {
    	return (
    	<View style={styles.container}>
    		<Accordion
				ref="content"
				activeSection={this.state.activeSection}
				sections={CONTENT}
				touchableComponent={TouchableOpacity}
				renderHeader={this.renderHeader}
				renderContent={this.renderContent}
				duration={300}
				onChange={this.setSection}
			/>	
		</View>
    	);
	}
}
const styles = StyleSheet.create({
  container: {
	width: Dimensions.get('window').width,
  },
  sectionHeader: {
  	marginTop: 10,
	width: '100%',
	height: 70,
	flexDirection: 'row',
	backgroundColor: theme.offWhite
  },
  functionHeading: {
	flex: 0.75,
	marginLeft: 15,
	justifyContent: 'center',
  },
  functionHeadingText: {
  	fontSize: 17,
  	fontWeight: '500',
  	color: theme.dark
  },
  headingIconContainer: {
  	flex: 0.25,
  	justifyContent: 'center',
  	alignItems: 'center' 
  },
  headingIcon: {
  	width: 22,
  	height: 22
  },
  sectionContent: {
  	width: '100%',
	height: 200,
	backgroundColor: 'lightgrey',
	alignItems: 'center'
  },
  functionDescription: {
  	flex: 0.7,
  	width: '90%',
  	marginTop: 10,
  	alignItems: 'center' 
  },
  functionDescriptionText: {
  	fontSize: 16,
  	fontWeight: '500',
  	color: theme.dark
  },
  buttonFlex: {
  	flex: 0.3,
  	width: '100%',
  	alignItems: 'flex-end',
  	justifyContent: 'flex-start'
  },
  runButton: {
  	marginRight: 15,
  	width: '30%',
  	height: '80%',
  	backgroundColor: 'green',
  	alignItems: 'center',
  	justifyContent: 'center',
  },
  runText: {
  	fontSize: 18,
  	fontWeight: '500',
  	color: theme.white
  }
});