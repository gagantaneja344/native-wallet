import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import theme from './common/theme';

export default class HomeScreen extends Component {
	gotoPinCheck = async () => {
    try {
      const value = await AsyncStorage.getItem('@PinSet');
      if (value !== null) {
        if( value === "true" ) {
          Actions.enterpin();
        }
        else {
          Actions.createpin();
        }
      }
      else {
          Actions.createpin();
     }
     } catch (error) {
       console.log(error)
     }
	}
	render() {
    	return (
    		<View style={styles.container}>
    			<View style={styles.upperFlex}>
    				<Text style={styles.upperFlexText}>Native Wallet</Text>
    			</View>
    			<View style={styles.lowerFlex}>
    				<TouchableOpacity style={styles.proceedButton} onPress={this.gotoPinCheck}>
    					<Text style={styles.proceedButtonText}>Proceed</Text>
    				</TouchableOpacity>
    			</View>
    		</View>
    	);
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.white,
    alignItems: 'center' 
  },
  upperFlex: {
  	flex: 0.8,
  	width: '100%',
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  upperFlexText: {
  	fontSize: 45,
  	fontWeight: '500',
  	color: theme.dark
  },
  lowerFlex: {
  	flex: 0.2,
  	width: '100%',
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  proceedButton: {
  	height: '50%',
  	width: '70%',
  	borderWidth: 1,
  	borderColor: theme.dark,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  proceedButtonText: {
  	fontSize: 24,
  	color: theme.dark,
  }
});
