import React from 'react';
import { Platform } from 'react-native';
export default {
  white: '#FFFFFF',
  dark: '#03131A',
  offWhite: '#f8f8f8',
};
