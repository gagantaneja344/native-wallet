import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions, TouchableOpacity, ScrollView, AsyncStorage, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import StatusBar from '../statusbar';
import AppStatusBar from '../appstatusbar';
import theme from '../theme';
import Done from '../images/done.png';
const VirgilCrypto =require('virgil-crypto');
const crypto = require('react-native-crypto');

const virgilCrypto = new VirgilCrypto.VirgilCrypto();

export default class CreatePin extends Component {
	constructor(props) {
    	super(props);
	    this.state = {
			Code: "",
			recievedCode: "",
			message: "hello"
		};
	}
	encrypt = (data, encryptionKeypair) => {
		const encryptedData = virgilCrypto.encrypt(data, encryptionKeypair.publicKey);
		const encryptedMessage = encryptedData.toString('base64');
		return encryptedMessage;
	}
	decrypt = (encryptedMessage, encryptionKeypair) => {
		const decryptedData = virgilCrypto.decrypt(encryptedMessage, encryptionKeypair.privateKey);
		const decryptedMessage = decryptedData.toString('utf8');
		return decryptedMessage;
	}
	storeData = async () => {
		const hmac = crypto.createHmac('sha256', this.state.Code);
		const hash = hmac.digest('hex');
		const encryptionKeypair = virgilCrypto.generateKeysFromKeyMaterial(hash);
		const encryptedMessage = this.encrypt("Hello", encryptionKeypair);
		try {
		    await AsyncStorage.setItem('@PinVerified', "true");
		    await AsyncStorage.setItem('@Data', encryptedMessage);
		  } catch (error) {
		    console.log(error)
		  }
	}
	setPin = async () => {
 		if( this.state.Code === this.props.pinCode ) {
 			this.storeData();
			try {
			    await AsyncStorage.setItem('@PinSet', "true");
			    Actions.mainscreen();
			  } catch (error) {
			    console.log(error)
			  }
 		}
 		else {
 			alert("Pin Did Not Match")
 		}
	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.headingFlex}>
					<Text style={styles.headingText}>Confirm Pin</Text>
				</View>
				<View style={styles.inputFlex}>
					<TextInput 
						style={styles.inputText}
	    				secureTextEntry
	    				keyboardAppearance="dark"
	    				keyboardType="number-pad"
	    				value={this.state.savedCode}
	    				maxLength={4}
	    				placeholder="X-X-X-X"
	    				placeholderTextColor="rgba(0,0,0,0.3)"
	    				onChangeText={(text) => this.setState({Code: text})}
					/>
				</View>
				<View style={styles.buttonFlex}>
					<TouchableOpacity onPress={this.setPin}>
						<Image source={Done} style={styles.doneButton} />
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor: 'white',
	},
	headingFlex: {
		flex: 0.25,
		alignItems: 'center',
		justifyContent: 'center'
	},
	headingText: {
		fontSize: 40,
	},
	inputFlex: {
		flex: 0.15,
		alignItems: 'center',
		justifyContent: 'center'
	},
	inputText: {
		borderBottomColor: "rgba(0,0,0,0.8)",
		borderBottomWidth: 3,
		textAlign: 'center',
		fontSize: 35,
		width: '50%',
		color: theme.dark
	},
	buttonFlex: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	doneButton: {
		width: 50,
		height: 50
	}
});